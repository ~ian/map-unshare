#include "map-unshare.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

static void dumpmaps(void) {
  FILE *maps = fopen("/proc/self/maps","r");
  int c;
  while ((c=getc(maps)) != EOF) putchar(c);
  assert(!ferror(maps) && !fclose(maps));
  assert(!fflush(stdout) && !ferror(stdout));
}

int main(void) {
  dumpmaps();
  map_unshare();
  dumpmaps();
  exit(0);
}
